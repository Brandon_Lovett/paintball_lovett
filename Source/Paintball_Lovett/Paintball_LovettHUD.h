// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "Paintball_LovettHUD.generated.h"

UCLASS()
class APaintball_LovettHUD : public AHUD
{
	GENERATED_BODY()

public:
	APaintball_LovettHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

	virtual void BeginPlay() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

	UPROPERTY(EditAnywhere, Category = "GUI")
		TSubclassOf<class UUserWidget> GUIWidgetClass;

};

