// Fill out your copyright notice in the Description page of Project Settings.

#include "NPC_AI_Controller.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Paintball_LovettCharacter.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Perception/AIPerceptionStimuliSourceComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "BlackBoardKeys.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "GameFramework/Character.h"


ANPC_AI_Controller::ANPC_AI_Controller(FObjectInitializer const& object_initializer)
{
	//Adds indicated behavior tree to AI character
	static ConstructorHelpers::FObjectFinder<UBehaviorTree> obj(TEXT("BehaviorTree'/Game/UE4_Mannequin_Mobile/NPC_AI_BT.NPC_AI_BT'"));
	if (obj.Succeeded())
	{
		behavior_tree = obj.Object;
	}
	behavior_tree_component = object_initializer.CreateDefaultSubobject<UBehaviorTreeComponent>(this, TEXT("BehaviorComp"));
	blackboard = object_initializer.CreateDefaultSubobject<UBlackboardComponent>(this, TEXT("BlackBoardComp"));

	setup_perception_system();
}
void ANPC_AI_Controller::BeginPlay()
{
	Super::BeginPlay();
	//Runs behavior tree when gameplay starts
	RunBehaviorTree(behavior_tree);
	behavior_tree_component->StartTree(*behavior_tree);
}

void ANPC_AI_Controller::Possess(APawn * const pawn)
{
	Super::Possess(pawn);
	//Initiallizes blackboard for behavior tree
	if (blackboard)
	{
		blackboard->InitializeBlackboard(*behavior_tree->BlackboardAsset);
	}
}

UBlackboardComponent* ANPC_AI_Controller::get_blackboard() const
{
	return blackboard;
}

void ANPC_AI_Controller::on_target_detected(AActor* actor, FAIStimulus const stimulus)
{
	//Changes behavior when player is sensed
	if (auto const ch = Cast<APaintball_LovettCharacter>(actor))
	{
		get_blackboard()->SetValueAsBool(bb_keys::can_see_player, stimulus.WasSuccessfullySensed());
	}
}

void ANPC_AI_Controller::setup_perception_system()
{
	//Create and Initialized Sight Object
	sight_config = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight Config"));
	SetPerceptionComponent(*CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("Perception Component")));
	sight_config->SightRadius = 500.0f;
	sight_config->LoseSightRadius = sight_config->SightRadius + 50.0f;
	sight_config->PeripheralVisionAngleDegrees = 90.0f;
	sight_config->SetMaxAge(5.0f);
	sight_config->AutoSuccessRangeFromLastSeenLocation = 900.0f;
	sight_config->DetectionByAffiliation.bDetectEnemies = true;
	sight_config->DetectionByAffiliation.bDetectFriendlies = true;
	sight_config->DetectionByAffiliation.bDetectNeutrals = true;

	//Add Sight Component to Perception Component
	GetPerceptionComponent()->SetDominantSense(*sight_config->GetSenseImplementation());
	GetPerceptionComponent()->OnTargetPerceptionUpdated.AddDynamic(this, &ANPC_AI_Controller::on_target_detected);
	GetPerceptionComponent()->ConfigureSense(*sight_config);
}

