// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Paintball_LovettCharacter.h"
#include "Components/CapsuleComponent.h"
#include "TimerManager.h"
#include "NPC_Character.generated.h"

UCLASS()
class PAINTBALL_LOVETT_API ANPC_Character : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ANPC_Character();

	UPROPERTY(VisibleAnywhere, Category = "Trigger Capsule")
		class UCapsuleComponent* TriggerCapsule;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
		void OnComponentBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnComponentEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
		void ResetTimer();

	UFUNCTION()
		void DealDamage();

	UFUNCTION()
		void StopTimer();

	APaintball_LovettCharacter* PlayerHit;

	FTimerHandle DamageLoop;

	UPROPERTY()
		bool IsDamagingPlayer;
	//Allows for changing the speed at which the player takes repeated damage
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Damage Timer")
		float DamageTimer;
	
};
