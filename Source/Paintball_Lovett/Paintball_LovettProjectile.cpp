// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Paintball_LovettProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "EngineUtils.h"

APaintball_LovettProjectile::APaintball_LovettProjectile() 
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &APaintball_LovettProjectile::OnHit);		// set up a notification for when this component hits something blocking

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;
	//Finds Decal for Paint splat
	static ConstructorHelpers::FObjectFinder<UMaterialInstance>Decal(TEXT("MaterialInstanceConstant'/Game/Shader/Paint_Splat_Inst.Paint_Splat_Inst'"));
	//Applies Decal Material if it is found
	if (Decal.Succeeded()) {
		DecalMat = Decal.Object;
	}

	Arrays();

}

void APaintball_LovettProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	DecalSize = UKismetMathLibrary::RandomFloatInRange(25.f, 60.f);
	Frame = UKismetMathLibrary::RandomIntegerInRange(0, 3);
	FrameToFloat = UKismetMathLibrary::Conv_IntToFloat(Frame);
	PlayerScore = Cast<APaintball_LovettCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	//If the Decal Material is set Decal will be applied to object hit by projectile with randomly selected size shape and color
	if (DecalMat) {
		DecalMatInstance = UMaterialInstanceDynamic::Create(DecalMat, this);
		DecalMatInstance->SetScalarParameterValue("Frame", FrameToFloat);
		DecalMatInstance->SetVectorParameterValue("Color", FLinearColor::MakeRandomColor());
	}
	//Attached decal to hit object to account for movement
	UGameplayStatics::SpawnDecalAttached(DecalMatInstance, FVector(DecalSize, DecalSize, DecalSize), Hit.GetComponent(), NAME_None, Hit.Location, Hit.Normal.Rotation(), EAttachLocation::KeepWorldPosition, 5.f);
	//Allows projectile to push objects which are simulating physics
	if (OtherActor != this && OtherComp->IsSimulatingPhysics())
	{
		OtherComp->AddImpulseAtLocation(ProjectileMovement->Velocity * 100.0f, Hit.ImpactPoint);
	}
	//Adds points for actors in indicated array
	for (int32 i = 0; i < CubeActors.Num(); i++)
	{
		if (OtherActor == CubeActors[i])
		{
			PlayerScore->LowScore();
		}
	}
	//Adds points for actors in indicated array
	for (int32 i = 0; i < NPCActors.Num(); i++)
	{
		if (OtherActor == NPCActors[i])
		{
			PlayerScore->HighScore();
		}
	}
	//Adds points for actors in indicated array
	for (int32 i = 0; i < RTActors.Num(); i++)
	{
		if (OtherActor == RTActors[i])
		{
			PlayerScore->MidScore();
		}
	}
	//Adds points for actors in indicated array
	for (int32 i = 0; i < STActors.Num(); i++)
	{
		if (OtherActor == STActors[i])
		{
			PlayerScore->MidScore();
		}
	}
	//Destroys the projectile after it hits anything
	Destroy();
}
//Arrays for various actors involved in points system
void APaintball_LovettProjectile::Arrays()
{
	if (GetWorld())
	{
		for (TActorIterator<ACubeActor> It(GetWorld(), ACubeActor::StaticClass()); It; ++It)
		{
			CubeHit = *It;
			if (CubeHit != NULL)
			{
				CubeActors.Add(CubeHit);
			}
		}

		for (TActorIterator<ANPC_Character> It(GetWorld(), ANPC_Character::StaticClass()); It; ++It)
		{
			NPCHit = *It;
			if (NPCHit != NULL)
			{
				NPCActors.Add(NPCHit);
			}
		}

		for (TActorIterator<ARunTimeCube> It(GetWorld(), ARunTimeCube::StaticClass()); It; ++It)
		{
			RTHit = *It;
			if (RTHit != NULL)
			{
				RTActors.Add(RTHit);
			}
		}

		for (TActorIterator<AShapeTestActor> It(GetWorld(), AShapeTestActor::StaticClass()); It; ++It)
		{
			STAHit = *It;
			if (STAHit != NULL)
			{
				STActors.Add(STAHit);
			}
		}
	}
}
