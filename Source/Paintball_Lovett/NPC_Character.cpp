// Fill out your copyright notice in the Description page of Project Settings.

#include "NPC_Character.h"


// Sets default values
ANPC_Character::ANPC_Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TriggerCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger Capsule"));
	TriggerCapsule->InitCapsuleSize(55.f, 96.0f);
	TriggerCapsule->SetCollisionProfileName(TEXT("Trigger"));
	TriggerCapsule->SetupAttachment(RootComponent);

	TriggerCapsule->OnComponentBeginOverlap.AddDynamic(this, &ANPC_Character::OnComponentBeginOverlap);
	TriggerCapsule->OnComponentEndOverlap.AddDynamic(this, &ANPC_Character::OnComponentEndOverlap);

	IsDamagingPlayer = true;
	DamageTimer = 1.0f;

}

// Called when the game starts or when spawned
void ANPC_Character::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ANPC_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ANPC_Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

//Called to begin player damage loop on overlap
void ANPC_Character::OnComponentBeginOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	PlayerHit = Cast<APaintball_LovettCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if ((OtherActor != nullptr) && (OtherActor == PlayerHit) && (OtherComp != nullptr)) {
		DealDamage();
	}
}
//Called to reset player damage loop for next overlap
void ANPC_Character::OnComponentEndOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
	PlayerHit = Cast<APaintball_LovettCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if ((OtherActor != nullptr) && (OtherActor == PlayerHit) && (OtherComp != nullptr))
	{
		StopTimer();
	}
}

//Loopsplayer damage
void ANPC_Character::ResetTimer()
{
	IsDamagingPlayer = true;
	GetWorldTimerManager().ClearTimer(DamageLoop);
	DealDamage();
}

//Damages the player repeatedly based on value of DamageTimer variable
void ANPC_Character::DealDamage()
{
	PlayerHit = Cast<APaintball_LovettCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (IsDamagingPlayer == true) {	
		IsDamagingPlayer = false;
		PlayerHit->PlayerDamage();
		GetWorld()->GetTimerManager().SetTimer(DamageLoop, this, &ANPC_Character::ResetTimer, DamageTimer, true);		
	}
}

//Stops the damage loop and resets it for when player triggers damage again
void ANPC_Character::StopTimer()
{
	GetWorldTimerManager().ClearTimer(DamageLoop);
	IsDamagingPlayer = true;
}


