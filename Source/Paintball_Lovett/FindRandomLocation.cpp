// Fill out your copyright notice in the Description page of Project Settings.

#include "FindRandomLocation.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AI/Navigation/NavigationSystem.h"
#include "NPC_AI_Controller.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Vector.h"
#include "BlackBoardKeys.h"

UFindRandomLocation::UFindRandomLocation(FObjectInitializer const& object_initializer)
{
	NodeName = TEXT("Find Random Location");
}


EBTNodeResult::Type UFindRandomLocation::ExecuteTask(UBehaviorTreeComponent & owner_comp, uint8 * node_memory)
{
	//Get AI Controller and NPC
	auto const cont = Cast<ANPC_AI_Controller>(owner_comp.GetAIOwner());
	auto const npc = cont->GetPawn();

	//Get NPC location and use as origin
	FVector const origin = npc->GetActorLocation();
	FNavLocation loc;

	//Get Nav sytem and generate random location on NavMesh
	UNavigationSystem* const nav_sys = UNavigationSystem::GetCurrent(GetWorld());
	if (nav_sys->GetRandomPointInNavigableRadius(origin, search_radius, loc, nullptr))
	{
		cont->get_blackboard()->SetValueAsVector(bb_keys::target_location, loc.Location);
	}

	FinishLatentTask(owner_comp, EBTNodeResult::Succeeded);
	return EBTNodeResult::Succeeded;
}
