// Fill out your copyright notice in the Description page of Project Settings.

#include "ShapeTestActor.h"


// Sets default values
AShapeTestActor::AShapeTestActor()
{
	//enables Tick for the actor so it can move
	PrimaryActorTick.bCanEverTick = true;
	//Adjustable variables to control the speed of the actors rotation
	PitchValue = 0.5f;
	YawValue = 0.5f;
	RollValue = 0.5f;
	//Creates the default mesh for the actor and makes the mesh the root component
	mesh = CreateDefaultSubobject < URuntimeMeshComponent >(TEXT("GeneratedMesh"));
	RootComponent = mesh;
	//Loads material to be added to actor later
	{
		static ConstructorHelpers::FObjectFinder <UMaterial>
			asset(TEXT("Material'/Game/Shader/MirrorMat.MirrorMat'"));
		m_dynamicMaterial = asset.Object;
	}

}

// Called when the game starts or when spawned
void AShapeTestActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AShapeTestActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//On every frame change rotation for smooth rotating actor
	FRotator NewRotation = FRotator(PitchValue, YawValue, RollValue);

	FQuat QuatRotation = FQuat(NewRotation);

	AddActorLocalRotation(QuatRotation, false, 0, ETeleportType::None);


}
//Called after the actor is spawned in the level
void  AShapeTestActor::PostActorCreated() {
	Super::PostActorCreated();
	GenerateTriMesh();
}//This is called when actor is already in level and map is opened.  
void  AShapeTestActor::PostLoad() {
	Super::PostLoad();
	GenerateTriMesh();
}

//Function creates an instance of the chosen material and then sets it to the actor mesh
void AShapeTestActor::PostInitializeComponents() {
	Super::PostInitializeComponents();
	// Create a dynamic material instance to swap in the fog texture.
	if (m_dynamicMaterial) {
		m_dynamicMaterialInstance = UMaterialInstanceDynamic::Create(m_dynamicMaterial, this);
	}
	// Set the dynamic material to the mesh.
	if (m_dynamicMaterialInstance) mesh->SetMaterial(0, m_dynamicMaterialInstance);
}

//Creates the mesh used for the triangles the actor will be made of
void AShapeTestActor::GenerateTriMesh() {
	TArray < FVector > Vertices;
	TArray < FVector > Normals;
	TArray < FRuntimeMeshTangent > Tangents;
	TArray < FVector2D > TextureCoordinates;
	TArray < int32 > Triangles;
	TArray < FColor > Colors;
	CreatePyramidMesh(FVector(50, 50, 50), Vertices, Triangles, Normals, TextureCoordinates, Tangents, Colors); // Create the mesh section, specifying collision.  
	mesh->CreateMeshSection(0, Vertices, Triangles, Normals, TextureCoordinates, Colors, Tangents, true);
}
//Creates Actor mesh based on how the created triangles are attached together
void AShapeTestActor::CreatePyramidMesh(FVector TriRadius, TArray < FVector > & Vertices, TArray < int32 > & Triangles, TArray < FVector > & Normals, TArray < FVector2D > & UVs, TArray < FRuntimeMeshTangent > & Tangents, TArray < FColor > & Colors) {
	FVector TriVerts[6];
	TriVerts[0] = FVector(-TriRadius.X, TriRadius.Y, TriRadius.Z);
	TriVerts[1] = FVector(TriRadius.X, TriRadius.Y, TriRadius.Z);
	TriVerts[2] = FVector(TriRadius.X, -TriRadius.Y, TriRadius.Z);
	TriVerts[3] = FVector(-TriRadius.X, -TriRadius.Y, TriRadius.Z);
	TriVerts[4] = FVector(-TriRadius.X, TriRadius.Y, -TriRadius.Z);
	TriVerts[5] = FVector(TriRadius.X, TriRadius.Y, -TriRadius.Z); // Generate triangles (from quads). 
	Triangles.Reset();
	const int32 NumVerts = 12; // 4 faces x 3 verts per face 
	Colors.Reset();
	Colors.AddUninitialized(NumVerts);
	for (int i = 0; i < NumVerts / 3; i++) {
		Colors[i * 3] = FColor(255, 0, 0);
		Colors[i * 3 + 1] = FColor(0, 255, 0);
		Colors[i * 3 + 2] = FColor(0, 0, 255);
	}
	Vertices.Reset();
	Vertices.AddUninitialized(NumVerts);
	Normals.Reset();
	Normals.AddUninitialized(NumVerts);
	Tangents.Reset();
	Tangents.AddUninitialized(NumVerts);
	//Creates the top of the static actor
	Vertices[0] = TriVerts[0];
	Vertices[1] = TriVerts[1];
	Vertices[2] = TriVerts[2];
	Triangles.Add(0);
	Triangles.Add(1);
	Triangles.Add(2);
	Normals[0] = Normals[1] = Normals[2] = FVector(0, 0, 1);
	Tangents[0] = Tangents[1] = Tangents[2] = FRuntimeMeshTangent(0.f, -1.f, 0.f);
	//Creates the left face of static actor 
	Vertices[3] = TriVerts[4];
	Vertices[4] = TriVerts[0];
	Vertices[5] = TriVerts[2];
	Triangles.Add(3);
	Triangles.Add(4);
	Triangles.Add(5);
	Normals[3] = Normals[4] = Normals[5] = FVector(-1, 0, 0);
	Tangents[3] = Tangents[4] = Tangents[5] = FRuntimeMeshTangent(0.f, -1.f, 0.f);
	////Creates the right face of static actor 
	Vertices[6] = TriVerts[1];
	Vertices[7] = TriVerts[0];
	Vertices[8] = TriVerts[4];
	Triangles.Add(6);
	Triangles.Add(7);
	Triangles.Add(8);
	Normals[6] = Normals[7] = Normals[8] = FVector(0, 1, 0);
	Tangents[6] = Tangents[7] = Tangents[8] = FRuntimeMeshTangent(-1.f, 0.f, 0.f);
	//Creates the rear face of static actor 
	Vertices[9] = TriVerts[2];
	Vertices[10] = TriVerts[1];
	Vertices[11] = TriVerts[4];
	Triangles.Add(9);
	Triangles.Add(10);
	Triangles.Add(11);
	Normals[9] = Normals[10] = Normals[11] = FVector(1, 0, 0);
	Tangents[9] = Tangents[10] = Tangents[11] = FRuntimeMeshTangent(0.f, 1.f, 0.f);
	UVs.Reset();
	UVs.AddUninitialized(NumVerts);
	UVs[0] = UVs[4] = UVs[8] = FVector2D(0.f, 0.f);
	UVs[1] = UVs[5] = UVs[9] = FVector2D(0.f, 1.f);
	UVs[2] = UVs[6] = UVs[10] = FVector2D(1.f, 1.f);
	UVs[3] = UVs[7] = UVs[11] = FVector2D(1.f, 0.f);
}


