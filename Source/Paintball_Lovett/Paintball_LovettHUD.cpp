// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Paintball_LovettHUD.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "UObject/ConstructorHelpers.h"

APaintball_LovettHUD::APaintball_LovettHUD()
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshairTexObj(TEXT("/Game/FirstPerson/Textures/FirstPersonCrosshair"));
	CrosshairTex = CrosshairTexObj.Object;
	//Sets GUI Widget
	static ConstructorHelpers::FClassFinder<UUserWidget> GUIWidget(TEXT("/Game/FirstPersonCPP/GUI/GUI"));

	if ((GUIWidget.Class != nullptr))
	{
		GUIWidgetClass = GUIWidget.Class;
	}
}

void APaintball_LovettHUD::BeginPlay()
{
	//Adds GUI to viewport when game starts
	UUserWidget * GUIRef = CreateWidget<UUserWidget>(GetWorld(), GUIWidgetClass);

	if (GUIRef) {
		GUIRef->AddToViewport(0);
	}
}
void APaintball_LovettHUD::DrawHUD()
{
	Super::DrawHUD();

	// Draw very simple crosshair

	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition( (Center.X),
										   (Center.Y + 20.0f));

	// draw the crosshair
	FCanvasTileItem TileItem( CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem( TileItem );
}
