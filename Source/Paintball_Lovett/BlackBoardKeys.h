#pragma once
#include "UObject/NameTypes.h"
#include "Containers/UnrealString.h"
#include "AI/Navigation/NavigationSystem.h"

namespace bb_keys
{
	TCHAR const * const target_location = TEXT("TargetLocation");
	TCHAR const * const can_see_player = TEXT("CanSeePlayer");
}