// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Paintball_Lovett : ModuleRules
{
	public Paintball_Lovett(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core",
                                                            "CoreUObject",
                                                            "Engine",
                                                            "InputCore",
                                                            "HeadMountedDisplay",
                                                            "PixelShader",
                                                            "ComputeShader",
                                                            "ProceduralMeshComponent",
                                                            "RuntimeMeshComponent",
                                                            "RHI",
                                                            "RenderCore",
                                                            "AIModule",
                                                            "GameplayTasks",
                                                            "UMG",
                                                            "Slate",
                                                            "SlateCore" });
    }
}
