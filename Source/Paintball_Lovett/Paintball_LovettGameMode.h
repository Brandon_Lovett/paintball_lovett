// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Paintball_LovettGameMode.generated.h"

UCLASS(minimalapi)
class APaintball_LovettGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APaintball_LovettGameMode();
};



