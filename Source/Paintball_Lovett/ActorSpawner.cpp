// Fill out your copyright notice in the Description page of Project Settings.

#include "ActorSpawner.h"


// Sets default values
AActorSpawner::AActorSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AActorSpawner::BeginPlay()
{
	Super::BeginPlay();
	//VAriables to get the location and rotation of the spawner
	const FVector Location = GetActorLocation();
	const FRotator Rotation = GetActorRotation();
	//Spawns the selected actor on runtime
	GetWorld()->SpawnActor<AActor>(ActorToSpawn, Location, Rotation);
	
}



