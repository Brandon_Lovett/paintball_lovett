// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Paintball_LovettGameMode.h"
#include "Paintball_LovettHUD.h"
#include "Paintball_LovettCharacter.h"
#include "UObject/ConstructorHelpers.h"

APaintball_LovettGameMode::APaintball_LovettGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = APaintball_LovettHUD::StaticClass();
}
