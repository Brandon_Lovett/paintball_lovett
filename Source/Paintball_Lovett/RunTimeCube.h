// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RuntimeMeshComponent.h" 
#include "Components/InstancedStaticMeshComponent.h"
#include "RunTimeCube.generated.h"

UCLASS()
class PAINTBALL_LOVETT_API ARunTimeCube : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARunTimeCube();
	//Initiallizes function for creating and setting instanced material for the actor
	virtual void PostInitializeComponents() override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void PostActorCreated() override;
	virtual void PostLoad() override;
	virtual void GenerateBoxMesh();
	virtual void CreateBoxMesh(FVector BoxRadius, TArray <FVector> & Vertices, TArray <int32> & Triangles, TArray <FVector> & Normals, TArray <FVector2D> & UVs, TArray <FRuntimeMeshTangent> & Tangents, TArray <FColor> & Colors);

	//Properties for loading and creating the matirial for the actor
	UPROPERTY()
		UMaterialInterface * m_dynamicMaterial;
	UPROPERTY()
		UMaterialInstanceDynamic * m_dynamicMaterialInstance;

private:
	UPROPERTY(VisibleAnywhere)
		URuntimeMeshComponent * mesh;

	
	
};
