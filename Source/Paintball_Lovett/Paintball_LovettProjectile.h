// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Paintball_LovettCharacter.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/DecalComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "UObject/ConstructorHelpers.h"
#include "GameFramework/Actor.h"
#include "CubeActor.h"
#include "NPC_Character.h"
#include "RunTimeCube.h"
#include "ShapeTestActor.h"
#include "PaintBall_LovettCharacter.h"
#include "Paintball_LovettProjectile.generated.h"

UCLASS(config=Game)
class APaintball_LovettProjectile : public AActor
{
	GENERATED_BODY()

	/** Sphere collision component */
	UPROPERTY(VisibleDefaultsOnly, Category=Projectile)
	class USphereComponent* CollisionComp;

	/** Projectile movement component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	class UProjectileMovementComponent* ProjectileMovement;

public:
	APaintball_LovettProjectile();

	/** called when projectile hits something */
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	/** Returns CollisionComp subobject **/
	FORCEINLINE class USphereComponent* GetCollisionComp() const { return CollisionComp; }
	/** Returns ProjectileMovement subobject **/
	FORCEINLINE class UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }

	UPROPERTY()
		UMaterialInstance* DecalMat;

	UPROPERTY()
		UMaterialInstanceDynamic* DecalMatInstance;

	UPROPERTY()
		float DecalSize;

	UPROPERTY()
		int Frame;

	UPROPERTY()
		float FrameToFloat;

	UFUNCTION()
		void Arrays();
//Pointers for actors involved in point system
	UPROPERTY()
		ACubeActor* CubeHit;

	UPROPERTY()
		APaintball_LovettCharacter* PlayerScore;

	UPROPERTY()
		ANPC_Character* NPCHit;

	UPROPERTY()
		ARunTimeCube* RTHit;

	UPROPERTY()
		AShapeTestActor* STAHit;
//Arrays for Point System
	TArray<ACubeActor*> CubeActors;

	TArray<ANPC_Character*> NPCActors;

	TArray<ARunTimeCube*> RTActors;

	TArray<AShapeTestActor*> STActors;
};

